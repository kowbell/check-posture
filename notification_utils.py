'''Utilities for displaying notifications on GNU/Linux, OS X, and Windows 10'''

import os, sys, platform
import logging
from enum import Enum # Jesus, I still have to import it a module?

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)

class Nenv(Enum):
    '''That is, (N)otifacation (En)vironment'''
    UNKNOWN         = [ "unknown" ]
    WINDOWS         = [ "windows" ]
    OSX             = [ "os x", "darwin" ]
    LINUX_CINNAMON  = [ "cinnamon" ]
    LINUX_GNOME     = [ "gnome" ]
    LINUX_KDE       = [ "kde", "plasma" ]
    LINUX_LXDE      = [ "lxde" ]
    LINUX_MATE      = [ "mate" ]
    LINUX_OPENBOX   = [ "openbox" ]
    LINUX_UNITY     = [ "unity" ]
    LINUX_XFCE4     = [ "xfce4" ]


def get_notification_environment():
    '''Attempts to get the notification environment from the current operating system

    :returns: NetworkEnvironment enum value, including UNKNOWN!
    '''

    os_environ = platform.system().lower()

    logger.info("Finding notification environment for platform {}...".format(os_environ))

    # Check if platform name is defined in Nenv
    for name, enum_member in Nenv.__members__.items():
        # Skip Linux for now - we handle that later
        if "LINUX_" in name:
            continue

        for keyword in enum_member.value:
            if keyword in os_environ:
                logger.debug( "Picking notification environment {} from OS platform {} (matched keyword {})".format(
                    enum_member, os_environ, keyword) )
                return enum_member

    # Check Linux desktop environment
    # TODO Handle this more cleanly; DESKTOP_SESSION appears to be a Linux only kinda deal
    desktop_session = os.environ.get("DESKTOP_SESSION")

    if desktop_session is None:
        logger.warn("Could not get DESKTOP_SESSION!")
        return Nenv.UNKNOWN
    else:
        desktop_session = desktop_session.lower()
    
    logger.debug("Finding notification environment for DESKTOP_SESSION {}".format(desktop_session))
    
    # Check if any Linux desktop name via reflection
    # God, this feels hacky
    for name, enum_member in Nenv.__members__.items():
        # Only use Linux variables
        if "LINUX_" not in name:
            continue

        for keyword in enum_member.value:
            if keyword in desktop_session:
                logger.debug( "Picking notification environment {} from DESKTOP_SESSION {} (matched keyword {})".format(
                    enum_member, desktop_session, keyword) )
                return enum_member

    logger.error("Could not find notification environment for DESKTOP_SESSION {}!".format(desktop_session))
    return Nenv.UNKNOWN

def notification(title, body = ''):
    '''Posts a notification with the given title and text

    TODO Add support for icons

    :return: False on failure, true on success
    '''

    nenv = get_notification_environment()

    if nenv is Nenv.UNKNOWN:
        logger.error("Failed to post notification - could not find network environment!")
        return False

    if nenv is Nenv.LINUX_KDE:
        os.system("notify-send '{}' '{}'".format(title, body))
        return True
    elif nenv is Nenv.OSX:
        os.system("osascript -e 'display notification \"{1}\" with title \"{0}\"'".format(title, body))
        return True

    logger.error("No notification command specified for environment {}!".format(nenv))
    return False

if __name__ == "__main__":
    print(get_notification_environment())

    notification("Notification Test", "Check out this sick message!\n"
        + "Check out this sick message on a new line!")
    #notification("Second Notification", "Take care of yourself! :)")