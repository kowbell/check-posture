import notification_utils
import time
import logging

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)

# Notification frequency in minutes
NOTIFY_FREQ = 15

if __name__ == "__main__":
    while True:
        notification_utils.notification("Check your posture!", "Back straight, shoulders back\n"
            + "Remember the three arches!")
        logger.debug("Sent notification! Sleeping for {} minutes...".format(NOTIFY_FREQ))
        time.sleep(NOTIFY_FREQ * 60)